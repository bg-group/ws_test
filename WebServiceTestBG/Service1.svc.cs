﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
namespace WebServiceTestBG
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public string GetDatav5(int value)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Info("HOLA MUNDO!!!");
            return string.Format("Tu ingresaste el valor: {0}", value);
            string information = "Code from branch Develop to branch MASTER";
        }
        public string getDataStringv5(string VALUE)
        {
            return string.Format("Tu ingresaste la siguiente cadena!!!: " + VALUE);
        }
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
